package com.example.aula3

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.round

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Botao calcular
        botao.setOnClickListener{
            val peso = input_peso.text.toString().toDouble(); // PESO
            val altura = input_altura.text.toString().toDouble() / 100; // ALTURA

            var imc = peso / ( altura * altura ) // CALCULO DO IMC

            // CONDIÇÕES DO IMC
            if(imc < 18.6){
                resultado.setText("Abaixo do Peso (${"%.2f".format(imc)})").toString();
            } else if( imc >= 18.6 && imc <= 24.9 ) {
                resultado.setText("Peso Ideal (${"%.2f".format(imc)})").toString();
            } else if ( imc >= 24.9 && imc <= 29.9 ) {
                resultado.setText("Levemente acima do peso (${"%.2f".format(imc)})").toString();
            } else if( imc >= 29.9 && imc <= 34.9 ){
                resultado.setText("Obesidade Grau I (${"%.2f".format(imc)})").toString();
            } else if( imc >= 34.9 && imc <= 39.9) {
                resultado.setText("Obesidade Grau II (${"%.2f".format(imc)})").toString();
            } else if( imc >= 40 ) {
                resultado.setText("Obesidade Grau III (${"%.2f".format(imc)})").toString();
            }
        }
    }

}
